class University{
    id;
    name;
    price;
    budget_places;
    paid_places;
    foreign_places;
    min_budget_score;
    min_paid_score;

    students = [];

    constructor(id, name, price, budget_places, paid_places, foreign_places, min_budget_score, min_paid_score, students){
        this.id = id;
        this.name = name;
        this.price = price;
        this.budget_places = budget_places;
        this.paid_places = paid_places;
        this.foreign_places = foreign_places;
        this.min_budget_score = min_budget_score;
        this.min_paid_score = min_paid_score;
        this.students = students;
    }
}

class Student{
    id;
    name;
    money;
    score;
    universityId;
    type;

    constructor(id, name, money, score, university_id, type){
        this.id = id;
        this.name = name;
        this.money = money;
        this.score = score;
        this.universityId = university_id;
        this.type = type;
    }
}

async function getUniversityWithStuds(){
    let response = await fetch('http://grnl.herokuapp.com/university')
    let json = await response.json()

    let data = json.data;

    let university = [];

    for(element of data){
        let {id, name, price, budget_places, paid_places, foreign_places, min_budget_score, min_paid_score} = element;
        university.push(new University(id, name, price, budget_places, paid_places, foreign_places, min_budget_score, min_paid_score));
    }

    for(i = 0; i < university.length; i++){
        let response2 = await fetch('http://grnl.herokuapp.com/university?id=' + university[i].id)
        let json2 = await response2.json()

        let dataOfStud = json2.data.students;

        let studentsOfUniversity = [];
        
        for(element of dataOfStud){
            let {id, name, money, score, university_id, type} = element;
            studentsOfUniversity.push(new Student(id, name, money, score, university_id, type));
        }

        university[i].students = studentsOfUniversity;
        console.log(university[i]);
    }
}
getUniversityWithStuds();